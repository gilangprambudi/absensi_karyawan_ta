<?php session_start(); ?>
<html>
<head>
    <title>Sistem Manajemen Absensi Karyawan Gramedia</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
          crossorigin="anonymous">
    <link rel="stylesheet" href="/assets/fontawesome/css/all.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>


    <style>
        .login_form {
            width: 40%;
            margin: auto
        }
        body{
            background-color: #e7e7f7;
        }
    </style>
</head>
<body>

<div class="container">
    <div class="row" style="margin-top: 120px;">
        <div class="col-md-8 offset-2">
            <div class="card" style="padding: 25px">
                <h2 style="text-align: center">Selamat Datang di Sistem Informasi
                    Absensi Toko Buku Gramedia Karawaci</h2>


                <div class="row">
                    <div class="col-md-6" style="padding: 30px;">
                        <div class="card" style="padding: 20px; margin: 10px">
                            <div style="text-align: center">Halaman Karyawan</div>
                            <a href="/views/login_karyawan.php" class="btn btn-primary" style="margin-top: 40px">Login
                                Disini</a>
                        </div>
                    </div>

                    <div class="col-md-6" style="padding: 30px">
                        <div class="card" style="padding: 20px; margin: 10px">
                            <div style="text-align: center">Halaman Admin</div>
                            <a href="/views/login_admin.php" class="btn btn-primary" style="margin-top: 40px">Login
                                Disini</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>

</body>
</html>
