<?php
session_start();
include("./../scripts/koneksi_db.php");

//get all attendance within this month
$attendances = [];
?>
<body>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
      integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
      crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<link rel="stylesheet" href="../assets/fontawesome/css/all.css">
<style>
    table tr th {
        padding: 4px;
    }

    table tr td {
        padding: 4px;
    }

    .live-clock {
        padding: 20px;
    }

</style>

<div class="row">
    <div class="col-md-5">
        <h2 style="padding: 20px; padding-top: 30px">
            <div class="card" style="display: inline-block; padding: 10px; ">
                <a href="dashboard_admin.php" style="color: black"><i class="fa fa-bars"></i></a>
            </div>
            Form Kehadiran Karyawan
        </h2>
    </div>
    <div class="offset-4 col-md-3">
        <div class="live-clock">
            <h1 style="text-align: right"></h1>
        </div>
    </div>
</div>


<div style="margin-top: 60px;">

    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-2">
                <div class="card" style="padding: 30px">
                    <h3 style="text-align: center">Masukkan NIK</h3>
                    <form method="POST" action="/scripts/absen_karyawan.php">
                        <input type="text" name="nip" class="form-control"
                               style="margin-top: 20px; font-size: 32px; text-align: center !important;">
                        <br>
                        <div style="color: #ff0000; margin-top: 20px; text-align: center">
                            <?php if (isset($_SESSION['attendance_employee_not_exist'])) {
                                unset($_SESSION['attendance_employee_not_exist']); ?>
                                *Karyawan dengan NIP tersebut tidak ditemukan
                            <?php } ?>

                            <?php if (isset($_SESSION['attendance_employee_has_no_schedule_today'])) {
                                unset($_SESSION['attendance_employee_has_no_schedule_today']); ?>
                                *Karyawan dengan NIP tersebut tidak memiliki jadwal hari ini
                            <?php } ?>

                            <?php if (isset($_SESSION['attendance_error'])) {
                                echo $_SESSION['attendance_error'];
                                unset($_SESSION['attendance_error']); ?>
                            <?php } ?>

                            <?php if (isset($_SESSION['isAbsen'])) {
                                echo "<span style='color: green'>Karyawan dengan NIK " . $_SESSION['nik_absen'] . " berhasil melakukan absensi</span>";
                                unset($_SESSION['isAbsen']);
                                unset($_SESSION['nik_absen']);
                                ?>
                            <?php } ?>

                        </div>
                        <br>
                        <div style="text-align: center">
                            <input style="width: 100%; " type="submit" value="Submit Kehadiran"
                                   class="btn btn-success">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        function getdate() {
            var today = new Date();
            var h = today.getHours();
            var m = today.getMinutes();
            var s = today.getSeconds();
            if (s < 10) {
                s = "0" + s;
            }

            if (h < 10) {
                h = "0" + h;
            }

            if (m < 10) {
                m = "0" + m;
            }

            $("h1").text(h + " : " + m + " : " + s);
            setTimeout(function () {
                getdate()
            }, 500);
        }

        getdate();
    });
</script>
</body>
</html>
