<?php ?>

<?php
session_start();
include("./../scripts/koneksi_db.php");
include("./../scripts/int_to_hour.php");

$shift_type = [];

$stmt = $pdo->query('SELECT * FROM `shift_type`');

$shift_type = [];
while ($row = $stmt->fetch()) {
    $shift_type[] = $row;
}

include "include/header.php";
?>

<div class="row">
    <div class="col-md-11">
        <h4 style="margin-top: 40px">Jadwal Shift</h4>

        <table class="table">
            <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Jam Masuk</th>
                <th>
                    Aksi
                </th>
            </tr>

            <?php for($i = 0; $i < sizeof($shift_type); $i++){?>
                <tr>
                    <td><?php echo $i + 1; ?></td>
                    <td><?php echo $shift_type[$i]['name'];?></td>
                    <td><?php echo intToHour($shift_type[$i]['time_start']);?> -
                        <?php echo intToHour($shift_type[$i]['time_end']);?></td>
                    <td>
                        <form method="post" action="/scripts/hapus_shift_type.php">
                            <input type="hidden" name="shift_id" value="<?php echo $shift_type[$i]['id']?>">
                            <input type="submit" class="btn btn-danger" value="Hapus">
                        </form>
                    </td>
                </tr>
            <?php } ?>
        </table>
    </div>
</div>

<div style="margin-top: 30px;" class="row">
    <div class="col-md-4">
        <h4><i class="fa fa-plus"></i> Tambah Data Shift</h4>
        <form action="/scripts/tambah_jadwal_shift.php" method="post">
            <div>
                <label>Nama Shift</label><br>
                <input type="text" name="name" class="form-control">
            </div>

            <div style="margin-top: 10px;">
                <label>Jam Masuk <span style="font-size: 12px">(gunakan angka, misal : 10:00 -> 1000)</span> </label><br>
                <input type="text" name="jamMasuk" class="form-control">

            </div>

            <div style="margin-top: 10px;">
                <label>Jam Keluar <span style="font-size: 12px">(gunakan angka, misal : 10:00 -> 1000)</span> </label><br>
                <input type="text" name="jamKeluar" class="form-control">

            </div>

            <div style="margin-top: 20px;">
                <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Daftarkan
                    Shift
                </button>
            </div>


        </form>
    </div>
</div>

<?php include "include/footer.php" ?>
