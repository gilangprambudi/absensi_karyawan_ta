<?php
session_start();
include("./../scripts/koneksi_db.php");
include("include/header.php");
include("./../scripts/int_time_calculator.php");
include "./../scripts/int_to_hour.php";
$date = date('d M, Y');

//get jumlah karyawan
$stmt = $pdo->query("SELECT * FROM `employee` where tipe_karyawan='internal'");
$employeeCount = 0;
while($row = $stmt->fetch()){
    $employeeCount += 1;
}

//get jumlah karyawan eksternal
$stmt = $pdo->query("SELECT * FROM `employee` where tipe_karyawan='eksternal'");
$employeeCountEx = 0;
while($row = $stmt->fetch()){
    $employeeCountEx += 1;
}

//get today's attendance
$date = date("Y-m-d");
$todayAttendance = [];
$stmt = $pdo->query('SELECT * FROM employee_attendance INNER JOIN employee
    ON employee_attendance.employee_id = employee.id 
    WHERE employee_attendance.attendance_date
        BETWEEN "' . $date . '" AND "'.$date.'"');
while($row = $stmt->fetch()){
    $todayAttendance[] = $row;
}

//for each user, calculate it's late time
$lateInMinutes= [];
$actualSchedules=[];
for ($i = 0; $i < sizeof($todayAttendance); $i++){

    $stmt = $pdo->query('SELECT shift_type.time_start FROM employee_shift INNER JOIN
    shift_type ON employee_shift.shift_id = shift_type.id
WHERE employee_shift.employee_id = '.$todayAttendance[$i]['employee_id'] .' AND employee_shift.date 
BETWEEN "'. $date .'" AND "' . $date . '"');

    if($row = $stmt->fetch()){
        $shouldAttendAt = $row['time_start'];
    }else{
        $shouldAttendAt = 0;
    }

    $actualSchedules[] = $shouldAttendAt;
    $lateInMinute = 0;
    if($todayAttendance[$i]['attendance_in'] > $shouldAttendAt){
        $lateInMinute = getMinuteRange($shouldAttendAt, $todayAttendance[$i]['attendance_in']);
    }
    //calculate late in minute
    $lateInMinutes[] = $lateInMinute;
}


//get pending permintaan tukar shift
$unseenShiftRequest = [];
$stmt = $pdo->query("select * FROM shift_change_request where approved = 0");
while ($row = $stmt->fetch()) {
    $unseenShiftRequest[] = $row;
}
?>


<div class="row" style="margin-top: 20px">
    <div class="col-md-12" style="text-align: right; padding-right: 40px">
       <div style="font-size: 28px">
           <?php echo $date?>
       </div>
    </div>

</div>
<div class="row" style="margin-top: 20px">
    <div class="col-md-4">
        <div class="card" style="padding: 30px">
            <div style="font-size: 18px">
                <i class="fa fa-users"></i> Jumlah Karyawan Internal
            </div>
            <div style="font-size: 32px; text-align: center; margin-top: 10px">
                <?php echo $employeeCount; ?>
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="card" style="padding: 30px">
            <div style="font-size: 18px">
                <i class="fa fa-users"></i> Jumlah Karyawan Eksternal
            </div>
            <div style="font-size: 32px; text-align: center; margin-top: 10px">
                <?php echo $employeeCountEx; ?>
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="card" style="padding: 30px">
            <div style="font-size: 20px">
                <i class="fa fa-list"></i> Permintaan Tukar Shift
            </div>
            <div style="font-size: 32px; text-align: center; margin-top: 10px">
                <?php echo sizeof($unseenShiftRequest);?>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-11" style="margin-top: 30px">
        <h3>Kehadiran hari ini</h3>
        <div style="float: right">
            <a href="/views/pdf/kehadiran_hari_ini.php" class="btn btn-primary" style="margin-bottom: 20px">
                <i class="fa fa-print"></i> Print Kehadiran Hari ini
            </a>
        </div>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th style="width: 50px">No</th>
                <th>Nama Karyawan</th>
                <th>Jadwal Jam Masuk</th>
                <th>Waktu Absensi</th>
                <th>Status Telat</th>
            </tr>
            </thead>
            <tbody>
            <?php if (sizeof($todayAttendance) < 1){?>
                <tr>
                    <td colspan="5"><i>Tidak ada kehadiran</i></td>
                </tr>
            <?php } ?>

                <?php for($i = 0; $i < sizeof($todayAttendance); $i++){?>
                    <tr>
                        <th><?php echo $i + 1;?></th>
                        <th><?php echo $todayAttendance[$i]['name'];?></th>
                        <th><?php echo intToHour( $actualSchedules[$i]);?></th>
                        <th><?php echo intToHour($todayAttendance[$i]['attendance_in']);?></th>
                        <th>
                            <?php if($lateInMinutes[$i] != 0){ ?>
                                <span style="color: red">
                                    <?php echo $lateInMinutes[$i] . " menit terlambat"; ?>
                                </span>
                            <?php }else{ ?>
                                <span style="color: green">Tepat Waktu</span>
                            <?php } ?>

                        </th>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>

</div>

</body>

</html>
