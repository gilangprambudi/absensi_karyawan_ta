<?php
session_start();
include("./../scripts/koneksi_db.php");
include("include/header_karyawan.php");
include("./../scripts/int_time_calculator.php");
include "./../scripts/int_to_hour.php";
$date = date('d M, Y');

$currentDate = date("Y-m-d");
$currentMonth = date("m");

$currentYear = date("Y");
$str = ltrim($currentMonth, '0');

$dateStart = $currentYear . "-" . $currentMonth . "-1";
$dateEnd = $currentYear . "-" . $currentMonth . "-31";

$karyawan_id = $_SESSION['karyawan']['id'];

//get shift by employee_id
//cari data admin di database
$stmt = $pdo->query("SELECT employee.name as emplname, employee_shift.id, 
employee_shift.date,
shift_type.name, shift_type.time_start, 
shift_type.time_end FROM employee_shift INNER JOIN
    shift_type ON
        shift_type.id = employee_shift.shift_id 
        INNER JOIN 
        employee ON employee.id = employee_shift.employee_id 
WHERE employee_shift.date BETWEEN '$dateStart' AND '$dateEnd' AND employee.id = $karyawan_id");

$shiftKaryawan = [];
$indexAt = 0;
while ($row = $stmt->fetch()) {
    $shiftKaryawan[] = $row;
    $shiftKaryawan[$indexAt]["human_time"] = intToHour($shiftKaryawan[$indexAt]["time_start"]) . " - " . intToHour($shiftKaryawan[$indexAt]["time_end"]);
    $indexAt++;
}


//get unseen shift request
$unseenShiftRequest = [];
$stmt = $pdo->query("select * FROM shift_change_request where employee_req_id=$karyawan_id AND seen=0");
while ($row = $stmt->fetch()) {
    $unseenShiftRequest[] = $row;
}
?>


<div class="container">
    <div class="row" style="margin-top: 40px">
        <div class="col-md-12">
            <h3>Selamat Datang <?php echo $_SESSION['karyawan']['name'] ?>!</h3>

            <div style="margin-top: 40px; margin-bottom: 40px;">
                <?php for ($i = 0; $i < sizeof($unseenShiftRequest); $i++) { ?>

                    <?php if ($unseenShiftRequest[$i]['approved'] == 1) { ?>
                        <div class="alert alert-success" style="width: 100%">
                            <i class="fa fa-bell"></i> Pengajuan Tukar Shift di terima admin
                            <form method="post" action="/scripts/seen_tukar_shift_notif.php" style="display: inline">
                                <input type="hidden" name="id" value="<?php echo $unseenShiftRequest[$i]['id']?>">
                                <input type="submit" class="btn btn-default" value="Tutup">
                            </form>
                        </div>
                    <?php } ?>

                    <?php if ($unseenShiftRequest[$i]['approved'] == -1) { ?>
                        <div class="alert alert-danger" style="width: 100%">
                            <i class="fa fa-bell"></i> Pengajuan Tukar Shift di tolak admin
                            <form method="post" action="/scripts/seen_tukar_shift_notif.php" style="display: inline">
                                <input type="hidden" name="id" value="<?php echo $unseenShiftRequest[$i]['id']?>">
                                <input type="submit" class="btn btn-default" value="Tutup">
                            </form>
                        </div>
                    <?php } ?>

                <?php } ?>
            </div>

            <h4 style="margin-top: 30px"><i class="fa fa-calendar-alt"></i> Jadwal Shift Bulan Ini</h4>
            <div style="float:right; margin-bottom: 20px">
                <a href="/views/pdf/shift_schedule.php?id=<?php echo $karyawan_id?>&date_start=<?php echo $dateStart?>&date_end=<?php echo $dateEnd?>"
                   class="btn btn-primary"><i class="fa fa-print"></i> Print Jadwal Bulan Ini</a>
            </div>
            <table class="table">
                <tr>
                    <th>No</th>
                    <th>Tanggal</th>
                    <th>Shift</th>
                </tr>
                <?php for ($i = 0; $i < sizeof($shiftKaryawan); $i++) { ?>
                    <tr>
                        <td><?php echo $i + 1; ?></td>
                        <td><?php echo $shiftKaryawan[$i]['date']; ?></td>
                        <td><?php echo $shiftKaryawan[$i]['name'] . " (" . $shiftKaryawan[$i]['human_time'] . ")"; ?></td>
                    </tr>
                <?php } ?>
            </table>
        </div>
    </div>
</div>
<h2></h2>

<?php
include("include/footer.php")
?>
