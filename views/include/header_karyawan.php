<html>
<head>
    <title>Absensi Karyawan Gramedia</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
          crossorigin="anonymous">
    <link rel="stylesheet" href="../../assets/fontawesome/css/all.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
</head>

<body>
<style>
    html, body{
        padding: 0px;
    }
    .sidebar{
        position: fixed;
        width: 320px;
        height: 100%;
        background-color: #2d4059;
        color: #fff;
    }

    .sidebar-menu ul{
        padding: 0px;
    }

    .sidebar-menu ul li{
        display: block;
        width: 100%;
        list-style: none;
        padding: 0;
    }

    .sidebar-menu ul li a{
        display: block;
        padding: 10px;
        color: #fff;
    }
</style>
<body>
<div class="container-fluid" style="padding: 0;">
    <div class="row">
        <div class="col-md-3">
            <div class="sidebar">
                <div style="padding: 10px">
                    <h4>Hello <b><?php echo $_SESSION['karyawan']['name']; ?></b></h4>
                    <h5>NIK : <b><?php echo $_SESSION['karyawan']['nip']; ?></b></h5><br>
                    <h6>Selamat Datang di Dashboard Absensi Karyawan PT.GRAMEDIA ASRI MEDIA (Karawaci) </h6>
                </div>

                <div class="sidebar-menu">
                    <ul>
                        <li><a href="/views/dashboard_karyawan.php"><i class="fa fa-home"></i> Home</a></li>
                        <li><a href="/views/karyawan-request-tukar-shift.php"><i class="fa fa-calendar"></i> Pengajuan Tukar Shift</a></li>
<!--                        <li><a href="/views/history_karyawan_tukar_shift.php"><i class="fa fa-history"></i> Histori Pengajuan Tukar Shift</a></li>-->
                        <li style="position: absolute; bottom: 30px; margin-left: 1px">
                            <a href="/scripts/logout_karyawan.php" style="color: #ea5455; font-size: 20px">
                                <i class="fa fa-sign-out-alt"></i> Logout
                            </a></li>
                    </ul>
                </div>
            </div>

        </div>

        <div class="col-md-9">

