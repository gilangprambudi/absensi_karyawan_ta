<?php
session_start();
include("./../scripts/koneksi_db.php");
include("./../scripts/int_to_hour.php");

$currentMonth = date("m");
$currentYear = date("Y");

//get semua daftar karyawan
//cari data admin di database
$stmt = $pdo->query('SELECT * FROM `employee`');

$employees = [];
while ($row = $stmt->fetch()) {
    $employees[] = $row;
}


//get tipe shift (pagi, malam, etc)
$stmt = $pdo->query('SELECT * FROM `shift_type`');
$shiftType = [];
while ($row = $stmt->fetch()) {
    $shiftType[] = $row;
}


$date = date("Y-m-d");

//check if date param is given
if (isset($_GET['date'])) {
    $date = $_GET['date'];
}

//get employee's shift based on date
$stmt = $pdo->query('SELECT employee_shift.shift_id, employee.id, employee.name, employee.nip FROM employee_shift INNER JOIN
    shift_type ON employee_shift.shift_id = shift_type.id
INNER JOIN employee on employee_shift.employee_id = employee.id
WHERE employee_shift.date BETWEEN "' . $date . '" AND "' . $date . '"');

$employeeOnShifts = [];

while ($row = $stmt->fetch()) {
    $employeeOnShifts[] = $row;
}

include "include/header.php";
?>
<link href='../assets/fullcalendar/lib/main.css' rel='stylesheet'/>
<script src='../assets/fullcalendar/lib/main.js'></script>
<script>

    document.addEventListener('DOMContentLoaded', function () {
        var calendarEl = document.getElementById('calendar');
        var calendar = new FullCalendar.Calendar(calendarEl, {
            initialView: 'dayGridMonth'
        });
        calendar.render();

        calendar.on('dateClick', function (info) {
            window.location = "manajemen_shift.php?date=" + info.dateStr;
        });
    });

</script>
<div style="padding: 20px">
    <div class="row" style="margin-bottom: 60px">
        <div class="col-md-12" style="margin-bottom: 30px">
            <h3>
                <i class="fa fa-calendar-alt"></i> Manajemen Jadwal Karyawan
            </h3>
        </div>
        <div class="col-md-12">
            <div class="card" style="padding: 50px">

                <div style="text-align: right; margin-bottom: 40px;">
                    <a class="btn btn-primary" href="/views/pdf/shift_schedule_all.php?month=<?php echo $currentMonth;?>&year=<?php echo $currentYear;?>">
                        <i class="fa fa-print"></i> Print Jadwal Bulan Ini</a>
                </div>

                <h5>Pilih tanggal untuk Melihat Jadwal</h5>
                <form action="/views/manajemen_shift.php">
                    <input type="date" class="form-control" style="margin-bottom: 30px" name="date">
                    <input type="submit" class="btn btn-primary" value="Pilih Tanggal" style="width: 20%">
                </form>
                <!--                    <div id='calendar' style="margin-top: 40px"></div>-->
                <div class="" style="margin-top: 50px">
                    <h3>Keterangan Shift : <span id="selectedDateShift">(<?php echo $date ?>)</span></h3>
                    <table class="table">
                        <thead>
                        <tr>
                            <th style="width: 30px">
                                No.
                            </th>
                            <th style="width: 150px">
                                Waktu
                            </th>
                            <th>
                                Karyawan Terjadwal
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php for ($i = 0; $i < sizeof($shiftType); $i++) { ?>
                            <tr>
                                <td>
                                    <?php echo $i + 1; ?>
                                </td>
                                <td>
                                    <?php
                                    echo $shiftType[$i]['name'];
                                    $startTime = intToHour($shiftType[$i]['time_start']);
                                    $endTime = intToHour($shiftType[$i]['time_end']);
                                    echo " (" . $startTime . " - " . $endTime . ")";
                                    ?>
                                </td>
                                <td>
                                    <?php for ($j = 0; $j < sizeof($employeeOnShifts); $j++) {
                                        if ($employeeOnShifts[$j]['shift_id'] == $shiftType[$i]['id']) {
                                            ?>
                                            -
                                            <a href="detail_karyawan.php?karyawan_id=<?php echo $employeeOnShifts[$j]['id'] ?>">
                                                <?php echo $employeeOnShifts[$j]['name'] . " " .
                                                    "(" . $employeeOnShifts[$j]['nip'] . ")"; ?>
                                            </a><br>
                                        <?php }
                                    } ?>
                                </td>
                            </tr>

                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>


        </div>
    </div>


    <div class="row">
        <div class="col-md-12" style="margin-top: 50px">
            <h5>Kelola Shift Karyawan</h5>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th style="width: 40px">No</th>
                    <th>NIK</th>
                    <th>Nama</th>
                    <th>Tenant</th>
                    <th style="width: 80px;">Aksi</th>
                </tr>
                </thead>
                <?php for ($i = 0; $i < sizeof($employees); $i++) {
                    $karyawanShiftURL = "manajemen_shift_karyawan.php?karyawan_id=" . $employees[$i]['id'];
                    ?>
                    <tr>
                        <td><?php echo $i + 1; ?></td>
                        <td><?php echo $employees[$i]['nip']; ?></td>
                        <td><?php echo $employees[$i]['name']; ?></td>
                        <td><?php echo $employees[$i]['tenant']; ?></td>
                        <td><a href="<?php echo $karyawanShiftURL; ?>">Kelola Shift</a></td>
                    </tr>
                <?php } ?>
            </table>
        </div>

    </div>
</div>
<?php include "include/footer.php" ?>
