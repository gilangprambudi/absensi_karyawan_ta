<?php
session_start();
include("./../scripts/koneksi_db.php");
include("include/header_karyawan.php");
include("./../scripts/int_time_calculator.php");
include "./../scripts/int_to_hour.php";
$date = date('d M, Y');
?>
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h2 style="margin-top: 20px">Form Pengajuan Tukar Shift</h2>
                <form style="margin-top: 40px" action="/scripts/request_tukar_shift.php" method="POST">
                    <label>Tanggal Ubah Shift</label>
                    <input type="date"  name="req_date" class="form-control date-tukar"><br>

                    <label>Shift Seharusnya</label>
                    <input type="text" disabled class="form-control shiftSeharusnya">

                    <h4 style="margin-top: 20px">Yang ditukar</h4>
                    <label>NIK</label><br>
                    <input type="text" name="nik" class="form-control nik_target"><br>
                    <button class="btn btn-primary checkNIK">Cari NIK</button>
                    <br><br>

                    <input type="text" disabled class="form-control namaTarget"><br>

                    <label>Shift Yang Ditukar pada tanggal tersebut</label><br>
                    <input type="text" name="nik" class="form-control shiftTargetSeharusnya" disabled><br>
                    <input type="hidden" class="target-id" name="target_id">
                    <input type="submit" value="Request Tukar Shift" class="btn btn-success">
                </form>
            </div>
        </div>
    </div>

    <script>

        //get user's current date
        $(".date-tukar").change(function () {
            var date = $(this).val();

            //get my shift from ajax
            $.get("/scripts/get_karyawan_shift.php?dateReq=" + date,
                function (data) {
                    var jsonData = JSON.parse(data)
                    if(jsonData["not_found"]){
                        $('.shiftSeharusnya').val("TIDAK ADA SHIFT");
                        return
                    }

                    console.log(jsonData);
                    $('.shiftSeharusnya').val(jsonData["name"] + " " + jsonData["human_time"]);
                }
            )
        })

        $(".checkNIK").click(function () {
            var nikTarget = $(".nik_target").val()

            var date = $(".date-tukar").val();
            if(date == undefined || date == null || date == ""){
                alert("Tolong isi tanggal dahulu")
                return false;
            }

            //get my shift from ajax
            $.get("/scripts/get_karyawan_shift.php?dateReq=" + date +"&karyawan_ditukar_nip="+nikTarget,
                function (data) {
                    var jsonData = JSON.parse(data)
                    if(jsonData["not_found"]){
                        $('.shiftTargetSeharusnya').val("TIDAK ADA SHIFT");
                        return
                    }

                    $('.shiftTargetSeharusnya').val(jsonData["name"] + " " + jsonData["human_time"]);
                    $(".namaTarget").val(jsonData["karyawan_name"]);
                    $(".target-id").val(jsonData["karyawan_id"]);
                }
            )
            return false;

        })

    </script>

<?php
include("include/footer.php")
?>
