<?php
session_start();
include("./../scripts/koneksi_db.php");
include("include/header_karyawan.php");
include("./../scripts/int_time_calculator.php");
include "./../scripts/int_to_hour.php";
$date = date('d M, Y');

$karyawan_id = $_SESSION['karyawan']['id'];

//get unseen shift request
$unseenShiftRequest = [];
$stmt = $pdo->query("select empl_req.name as empl_req_name, 
                                    shift_type_req.req_start_time 
                                    shift_type_req.req_end_time FROM shift_change_request 
INNER JOIN employee empl_req on empl_req.id = shift_change_request.employee_req_id
INNER JOIN shift_type shift_type_req ON shift_type_req.id = 
where employee_req_id=$karyawan_id");
while ($row = $stmt->fetch()) {
    $unseenShiftRequest[] = $row;
}
?>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3 style="margin-top: 50px"><i class="fa fa-history"></i> History Pengajuan Tukar Shift</h3>
            </div>
        </div>

        <div class="row" style="margin-top: 50px">
            <div class="col-md-12">
                <table class="table">
                    <tr>
                        <th>No</th>
                        <th>Tanggal</th>
                        <th>Shift Awal</th>
                        <th>Tukar ke Shift</th>
                        <th>NIK Karyawan</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                </table>
            </div>
        </div>
    </div>
<?php
include("include/footer.php")
?>
