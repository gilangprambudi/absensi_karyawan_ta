<?php
session_start();
include("./../scripts/koneksi_db.php");
include("./../scripts/int_to_hour.php");
//get semua daftar karyawan
//cari data admin di database

//get data karyawan by id

$karyawanID = $_GET['karyawan_id'];

//get semua daftar karyawan
//cari data admin di database
$stmt = $pdo->query('SELECT * FROM `employee` WHERE id = ' . $karyawanID);

$employee = null;

if ($row = $stmt->fetch()) {
    $employee = $row;
}

//get tipe shift (pagi, malam, etc)
$stmt = $pdo->query('SELECT * FROM `shift_type`');
$shiftType = [];
while ($row = $stmt->fetch()) {
    $shiftType[] = $row;
}

//get current month
$currentMonth = date('m');
$currentYear = date("Y");
$thisMonthStart = $currentYear . "-" . $currentMonth . "-" . 1;
$thisMonthEnd = $currentYear . "-" . $currentMonth . "-" . 31;


//get employee shift
$stmt = $pdo->query("SELECT employee_shift.id, shift_type.name, employee_shift.date, shift_type.time_start, shift_type.time_end
FROM employee_shift INNER JOIN shift_type ON 
employee_shift.shift_id = shift_type.id WHERE employee_shift.employee_id = '$karyawanID' AND
 date between '$thisMonthStart' AND '$thisMonthEnd' ORDER BY employee_shift.date");
$employeeShifts = [];
while ($row = $stmt->fetch()) {
    $employeeShifts[] = $row;
}
include "include/header.php";
?>
    <h3 style="margin-top: 50px">Kelola Shift Karyawan</h3>
    <h4>Nama Karyawan : <b><?php echo $employee['name'] . " - NIK : " . $employee['nip']; ?></b></h4>
    <h5>Tenant : <?php echo $employee['tenant']; ?></h5>
    <div class="row" style="margin-top: 40px">
        <div class="col-md-10">
            <h3>Jadwal Masuk Bulan Ini</h3>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th style="width: 30px">No.</th>
                    <th>Tanggal</th>
                    <th>Shift</th>
                    <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                <?php for ($i = 0; $i < sizeof($employeeShifts); $i++) { ?>
                    <tr>
                        <td><?php echo $i + 1; ?></td>
                        <td><?php echo $employeeShifts[$i]['date']; ?></td>
                        <td>
                            <?php
                            echo $employeeShifts[$i]['name'];
                            $startTime = intToHour($employeeShifts[$i]['time_start']);
                            $endTime = intToHour($employeeShifts[$i]['time_end']);
                            echo " (" . $startTime . " - " . $endTime . ")";
                            ?>
                        </td>
                        <td>
                            <form action="/scripts/delete_shift_karyawan.php" method="post">
                                <input type="hidden" value="<?php echo $employeeShifts[$i]['id']?>"
                                       name="shift_id">
                                <input type="hidden" value="<?php echo $karyawanID?>" name="karyawan_id">
                                <input type="submit" class="btn btn-danger" value="Hapus">
                            </form>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>

    <div class="row" style="margin-top: 80px;">
        <div class="col-md-10">
            <h4><i class="fa fa-plus"></i> Tambah Shift</h4>
            <form action="../scripts/tambah_shift.php" method="post">
                <label>Tanggal Masuk</label>
                <input type="date" class="form-control" name="shiftDate">

                <label style="margin-top: 25px">Shift</label>
                <select class="form-control" name="shiftType">
                    <?php for ($i = 0; $i < sizeof($shiftType); $i++) { ?>
                        <option value="<?php echo $shiftType[$i]['id']; ?>">
                            <?php
                            echo $shiftType[$i]['name'];
                            $startTime = intToHour($shiftType[$i]['time_start']);
                            $endTime = intToHour($shiftType[$i]['time_end']);
                            echo " (" . $startTime . " - " . $endTime . ")";
                            ?>
                        </option>
                    <?php } ?>
                </select>
                <input type="hidden" name="employeeId" value="<?php echo $karyawanID; ?>">

                <div style="color: red; height: 30px">
                    <?php if(isset($_SESSION['date_occupied'])){
                        $_SESSION['date_occupied'] = null;?>
                        Penambahan jadwal gagal : Karyawan telah memiliki jadwal pada tanggal tersebut
                    <?php } ?>
                </div>
                <input type="submit" value="Tambah Shift" class="btn btn-primary" style="margin-top: 30px">
            </form>
        </div>
    </div>
<?php include "include/footer.php" ?>
