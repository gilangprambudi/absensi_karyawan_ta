<?php
session_start();

include '../../scripts/int_time_calculator.php';

require_once '../../vendor/autoload.php';
$mpdf = new \Mpdf\Mpdf();
include("../../scripts/koneksi_db.php");
include("../../scripts/int_to_hour.php");

$karyawan_id = $_GET['karyawan_id'];
$month = $_GET['month'];
$year = $_GET['year'];

$dateStart = $year."-".$month."-1";
$dateEnd = $year."-".$month."-31";


//get data karyawan
$stmt = $pdo->query('SELECT * FROM `employee` WHERE id = ' . $karyawan_id);

$employee = null;

if ($row = $stmt->fetch())
{
    $employee = $row;
}


$userShift = [];
$stmtShift = $pdo->query("SELECT * FROM `employee_shift` 
INNER JOIN `shift_type` ON 
shift_type.id = employee_shift.shift_id 
WHERE employee_id = '$karyawan_id'
AND date BETWEEN '$dateStart' AND '$dateEnd' ORDER BY employee_shift.date");
while ($row = $stmtShift->fetch())
{
    $userShift[] = $row;
}


//get data absensi karyawan
$attendances = [];
$stmtAttendance = $pdo->query("SELECT * FROM `employee_attendance` 
WHERE employee_id='$karyawan_id' AND attendance_date BETWEEN '$dateStart' AND '$dateEnd'");
while ($row = $stmtAttendance->fetch())
{
    $attendances[] = $row;
}


$content = '
<h3>Kehadiran Karyawan</h3>

<table>
    
    <tr>
        <td>Nama Karyawan</td><td>:</td><td>'. $employee['name'] . ' (' . $employee['nip'] . ')</td>
    </tr>
    
    <tr>
      <td>Periode</td><td>:</td><td>'. $dateStart .' - ' . $dateEnd . '</td>
    </tr>
    
    <tr>
        <td>Tahun</td><td>:</td><td>'.$year.'</td>
    </tr>
</table>
<style>
   .sched_tab {
        margin-top: 1.3cm;
        font-size: 10px;
   }

   .sched_tab td{
        border: 1px solid black;
        padding: 5px;
    }
    
    .sched_tab__header{
        background-color: aquamarine;
    }
</style>
<table class="sched_tab" cellspacing="0" width="100%">
    <tr class="sched_tab__header">
        <td style="width: 10%">No</td><td style="width: 20%">Tanggal</td>
        <td>Shift</td><td>Jam Masuk</td><td>Jam Keluar</td><td style="text-align: center">Total Jam Kerja</td><td>Telat (Menit)</td>
    </tr>';

//compare attendance with user schedule
$sumHourIn = 0;
$sumLateMinute = 0;

for ($i = 0; $i < sizeof($userShift); $i++){
    //check if there is any atendance in the current date

    $minuteLate=0;
    $isAttend = 0; //0 : Not yet, 1 : attended, -1 : not attended

    $minuteHour = getMinuteRange($userShift[$i]['time_end'], $userShift[$i]['time_start']);

    $isLate = false;
    $attendAt = 0;
    $attendOutAt = 0;

    for($j = 0; $j < sizeof($attendances); $j++){
        if($userShift[$i]['date'] == $attendances[$j]['attendance_date']){
            $isAttend = 1;
            //calculate minute late
            if($userShift[$i]['time_start'] < $attendances[$j]['attendance_in']){
                $isLate = true;
            }
            $attendAt = intToHour($attendances[$j]['attendance_in']);
            $attendOutAt = intToHour($attendances[$j]['attendance_out']);
            $minuteLate = getMinuteRange($userShift[$i]['time_start'], $attendances[$j]['attendance_in']);

            $minuteHour = getMinuteRange($attendances[$j]['attendance_out'], $attendances[$j]['attendance_in']);
        }
    }

    if($isAttend == 0){
        $dateNow = new DateTime();
        $dateSched = new DateTime($userShift[$i]['date']);
        $in =  $dateSched->diff($dateNow);
        $dayDiff = $in->format('%R%a');
        if($dayDiff > 0){
            //indicate user is absent
            $isAttend = -1;
        }
    }

    if ($isAttend == -1){
        $workHour = -round($minuteHour/60);
    }

    if ($isAttend == 1){
        $minuteHour = $minuteHour;
        $workHour = round($minuteHour/60);
        $sumHourIn += $workHour;
    }

    if($isAttend == 0){
        $workHour = "NA";
    }
    $sumLateMinute += $minuteLate;

    $no = $i;
    $no = $no+1;
    $dateSched = $userShift[$i]['date'];
    $userShiftData = $userShift[$i]['name'] . " ( " . intToHour($userShift[$i]['time_start']) .  "-" .
        intToHour($userShift[$i]['time_end']) . " )";
    if(!$isLate){
        $minuteLate = 0;
    }

    $content .= "<tr><td>$no</td><td>$dateSched</td><td>$userShiftData</td><td>$attendAt</td><td>$attendOutAt</td>
       <td style='text-align: center'>$workHour</td></tr><td style='text-align: center'>$minuteLate</td>";
}


$content .= "
<tr>
    <td colspan='5'>Total</td>
     <td style='text-align: center'>$sumHourIn Jam</td>
    <td style='text-align: center'>$sumLateMinute Menit</td>
</tr>
</table>";


$mpdf->WriteHTML($content);
$mpdf->Output();
