<?php

require_once '../../vendor/autoload.php';

include '../../scripts/koneksi_db.php';
include '../../scripts/int_to_hour.php';
$mpdf = new \Mpdf\Mpdf();

//get user's Data
$dateStart = $_GET['date_start'];
$dateEnd = $_GET['date_end'];
$karyawan_id = $_GET['id'];

//get karyawan data
//get semua daftar karyawan
//cari data admin di database
$stmt = $pdo->query('SELECT * FROM `employee` WHERE id = ' . $karyawan_id);

$employee = null;

if ($row = $stmt->fetch())
{
    $employee = $row;
}


$content = '
<h3>Jadwal Shift Karyawan</h3>

<table>
    
    <tr>
        <td>Nama Karyawan</td><td>:</td><td>'. $employee['name'] . ' (' . $employee['nip'] . ')</td>
    </tr>
    
    <tr>
      <td>Periode</td><td>:</td><td>'. $dateStart .' - ' . $dateEnd . '</td>
    </tr>
    
    <tr>
        <td>Tahun</td><td>:</td><td>2020</td>
    </tr>
</table>
<style>
   .sched_tab {
        margin-top: 1.3cm;
        font-size: 10px;
   }

   .sched_tab td{
        border: 1px solid black;
        padding: 5px;
    }
    
    .sched_tab__header{
        background-color: aquamarine;
    }
</style>
<table class="sched_tab" cellspacing="0" width="100%">
    <tr class="sched_tab__header">
        <td style="width: 10%">No</td><td style="width: 40%">Tanggal</td><td>Shift</td><td>Jam Masuk</td>
    </tr>';


//iterate through karyawan shift

//get shift by employee_id
//cari data admin di database
$dateStart = "2020-10-1";
$stmt = $pdo->query("SELECT employee.name as emplname, employee_shift.id, 
employee_shift.date,
shift_type.name, shift_type.time_start, 
shift_type.time_end FROM employee_shift INNER JOIN
    shift_type ON
       shift_type.id =employee_shift.shift_id INNER JOIN 
        employee ON employee.id = employee_shift.employee_id 
WHERE employee_shift.date BETWEEN '$dateStart' AND '$dateEnd' AND employee.id = $karyawan_id");

$shiftKaryawan = null;
$indexAt = 0;
while($row = $stmt->fetch()){
    $shiftKaryawan[] = $row;
    $shiftKaryawan[$indexAt]["human_time"] = intToHour($shiftKaryawan[$indexAt]["time_start"]) . " - " . intToHour($shiftKaryawan[$indexAt]["time_end"]);
    $indexAt++;
}

if($shiftKaryawan == null){
    $response = ["not_found" => true];
    echo "<div>
            <h3 style='margin: auto;'>Jadwal Belum Terbuat</h3>
           </div>";
    return;
}




//iterate through shift karyawan
for($i = 0; $i < sizeof($shiftKaryawan); $i++){
    $num = $i+1;
    $content .=
        '<tr>
<td>' . $num . '</td>
<td>'.$shiftKaryawan[$i]['date'].'</td>
<td>'. $shiftKaryawan[$i]['name'] . '</td>
<td>'. $shiftKaryawan[$i]['human_time'] . '</td>
</tr>';
}

$content .= '</table>';


$mpdf->WriteHTML($content);
$mpdf->Output();
