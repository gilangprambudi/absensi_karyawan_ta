<?php
session_start();
include("./../../scripts/koneksi_db.php");
include("./../..//scripts/int_time_calculator.php");
include "./../../scripts/int_to_hour.php";
require_once '../../vendor/autoload.php';

$date = date('d M, Y');

//get jumlah karyawan
$stmt = $pdo->query("SELECT * FROM `employee` where tipe_karyawan='internal'");
$employeeCount = 0;
while($row = $stmt->fetch()){
    $employeeCount += 1;
}

//get jumlah karyawan eksternal
$stmt = $pdo->query("SELECT * FROM `employee` where tipe_karyawan='eksternal'");
$employeeCountEx = 0;
while($row = $stmt->fetch()){
    $employeeCountEx += 1;
}

//get today's attendance
$date = date("Y-m-d");
$todayAttendance = [];
$stmt = $pdo->query('SELECT * FROM employee_attendance INNER JOIN employee
    ON employee_attendance.employee_id = employee.id 
    WHERE employee_attendance.attendance_date
        BETWEEN "' . $date . '" AND "'.$date.'"');
while($row = $stmt->fetch()){
    $todayAttendance[] = $row;
}

//for each user, calculate it's late time
$lateInMinutes= [];
$actualSchedules=[];
for ($i = 0; $i < sizeof($todayAttendance); $i++){

    $stmt = $pdo->query('SELECT shift_type.time_start, shift_type.time_end FROM employee_shift INNER JOIN
    shift_type ON employee_shift.shift_id = shift_type.id
WHERE employee_shift.employee_id = '.$todayAttendance[$i]['employee_id'] .' AND employee_shift.date 
BETWEEN "'. $date .'" AND "' . $date . '"');

    if($row = $stmt->fetch()){
        $shouldAttendAt = $row['time_start'];
        $shouldLeaveAt = $row['time_end'];
    }else{
        $shouldAttendAt = 0;
    }

    $actualSchedules[] = intToHour($shouldAttendAt) . " - " . intToHour($shouldLeaveAt);
    $lateInMinute = 0;
    if($todayAttendance[$i]['attendance_in'] > $shouldAttendAt){
        $lateInMinute = getMinuteRange($shouldAttendAt, $todayAttendance[$i]['attendance_in']);
    }
    //calculate late in minute
    $lateInMinutes[] = $lateInMinute;
}


//get pending permintaan tukar shift
$unseenShiftRequest = [];
$stmt = $pdo->query("select * FROM shift_change_request where approved = 0");
while ($row = $stmt->fetch()) {
    $unseenShiftRequest[] = $row;
}


$content = '
<h3>Jadwal Kehadiran Hari Ini</h3>
<h4>
    Tanggal : ' . $date . ' 
</h4>
<style>
   .sched_tab {
        margin-top: 1.3cm;
        font-size: 10px;
   }

   .sched_tab td{
        border: 1px solid black;
        padding: 5px;
    }
    
    .sched_tab__header{
        background-color: aquamarine;
    }
</style>

<table class="sched_tab" cellspacing="0" width="100%">
    <tr class="sched_tab__header">
        <td>No</td>
        <td>Nama Karyawan</td>
        <td>Shift</td>
        <td>Jam Masuk</td>
        <td>Jam Keluar</td>
        <td>Status Telat</td>
    </tr>';

 for($i = 0; $i < sizeof($todayAttendance); $i++){
     $content .= '<tr>';

     $content .= "<td>" . ($i + 1) . "</td>";
     $content .= "<td>" . $todayAttendance[$i]['name'] . "</td>";
     $content .= "<td>" . $actualSchedules[$i]  . "</td>";
     $content .= "<td>" . intToHour($todayAttendance[$i]['attendance_in']) . "</td>";
     $content .= "<td>" . intToHour($todayAttendance[$i]['attendance_out']) . "</td>";
      if($lateInMinutes[$i] != 0){
          $content .= "<td>" . $lateInMinutes[$i] . " menit terlambat" . "</td>";
      }else{
          $content .= "<td> Tepat Waktu </td>";
      }
 }

$content .= "</table>";


$mpdf = new \Mpdf\Mpdf();

$mpdf->WriteHTML($content);
$mpdf->Output();
?>
