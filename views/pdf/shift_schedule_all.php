<?php

require_once '../../vendor/autoload.php';

include '../../scripts/koneksi_db.php';
include '../../scripts/int_to_hour.php';

$mpdf = new \Mpdf\Mpdf();

//get user's Data
$month = $_GET['month'];
$year = $_GET['year'];

$dateStart = $year . "-" . $month . "-1";
$dateEnd = $year . "-" . $month . "-31";

$content = '
<h3>Jadwal Shift Karyawan (All)</h3>
<style>
   .sched_tab {
        margin-top: 1.3cm;
        font-size: 7pt;
   }

   .sched_tab td{
        border: 1px solid black;
        padding: 5px;
    }

    .sched_tab__header{
        background-color: aquamarine;
    }
</style>
<table class="sched_tab" cellspacing="0" width="100%">
    <tr class="sched_tab__header">
        <td style="width: 10%">No</td><td style="width: 25%">Tanggal</td><td style="width: 40%">Karyawan - NIK</td><td>Shift</td><td>Jam Masuk</td>
    </tr>';


//iterate through karyawan shift

//get shift by employee_id
//cari data admin di database
$stmt = $pdo->query("SELECT employee.name as emplname, employee.nip, employee_shift.id,
employee_shift.date,
shift_type.name, shift_type.time_start,
shift_type.time_end FROM employee_shift INNER JOIN
    shift_type ON
        shift_type.id =employee_shift.shift_id INNER JOIN
        employee ON employee.id = employee_shift.employee_id
WHERE employee_shift.date BETWEEN '$dateStart' AND '$dateEnd' ORDER BY employee_shift.date ASC");

$shiftKaryawan = null;
$indexAt = 0;
while ($row = $stmt->fetch()) {
    $shiftKaryawan[] = $row;
    $shiftKaryawan[$indexAt]["human_time"] = intToHour($shiftKaryawan[$indexAt]["time_start"]) . " - " . intToHour($shiftKaryawan[$indexAt]["time_end"]);
    $indexAt++;
}

if ($shiftKaryawan == null || $shiftKaryawan == "") {
    $response = ["not_found" => true];
    echo "<div>
            <h3 style='text-align: center; margin-top: 50px'>Jadwal Belum Terbuat!</h3>
            <a href='/views/laporan.php' style='text-align: center; width: 100%; display: block'>Klik Untuk Kembali Ke Halaman Laporan</a>
           </div>
    ";
    return;
}


//iterate through shift karyawan
for ($i = 0; $i < sizeof($shiftKaryawan); $i++) {
    $num = $i + 1;
    $content .=
        '<tr>
<td>' . $num . '</td>
<td>' . $shiftKaryawan[$i]['date'] . '</td>
<td>' . $shiftKaryawan[$i]['emplname'] . ' (' . $shiftKaryawan[$i]['nip'] . ')</td>
<td>' . $shiftKaryawan[$i]['name'] . '</td>
<td>' . $shiftKaryawan[$i]['human_time'] . '</td>
</tr>';
}

$content .= '</table>';


$mpdf->WriteHTML($content);
$mpdf->Output();
