<?php
session_start();
include("./../scripts/koneksi_db.php");
//get semua daftar karyawan
//cari data admin di database
$stmt = $pdo->query('SELECT * FROM `employee`');

$employees = [];
while ($row = $stmt->fetch()) {
    $employees[] = $row;
}

include "include/header.php";
?>
    <div class="content" style="padding: 20px">
        <h2><i class="fa fa-users"></i> List Karyawan</h2>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th style="width: 40px">No</th>
                <th>NIK</th>
                <th>Nama</th>
                <th>Tenant</th>
                <th style="width: 80px;">Aksi</th>
            </tr>
            </thead>
            <?php for ($i = 0; $i < sizeof($employees); $i++) {
                $karyawanDetailURL = "detail_karyawan.php?karyawan_id=" . $employees[$i]['id'];
                ?>
                <tr>
                    <td><?php echo $i + 1; ?></td>
                    <td><?php echo $employees[$i]['nip']; ?></td>
                    <td><?php echo $employees[$i]['name']; ?></td>
                    <td><?php echo $employees[$i]['tenant']; ?></td>
                    <td><a href="<?php echo $karyawanDetailURL ?>" class="btn btn-primary">Kelola</a>
                         <br><br>
                        <form action="/scripts/delete_karyawan.php" method="post">
                            <input type="hidden" name="karyawan_id" value="<?php echo  $employees[$i]['id']; ?>">
                            <input type="submit" value="Hapus" class="btn btn-danger">
                        </form>
                    </td>
                </tr>
            <?php } ?>
        </table>


        <div style="margin-top: 30px;" class="row">
            <div class="col-md-4">
                <h4><i class="fa fa-plus"></i> Tambah Data Karyawan</h4>
                <form action="/scripts/tambah_karyawan.php" method="post" enctype="multipart/form-data">
                    <div>
                        <label>NIK</label><br>
                        <input type="text" name="nip" class="form-control">
                    </div>

                    <div style="margin-top: 10px;">
                        <label>Name</label><br>
                        <input type="text" name="name" class="form-control">

                    </div>

                    <div style="margin-top: 10px;">
                        <label>Tenant</label><br>
                        <input type="text" name="tenant" class="form-control">
                    </div>

                    <div style="margin-top: 10px;">
                        <label>KTP</label><br>
                        <input type="file" name="foto_ktp" class="form-control">
                    </div>

                    <div style="margin-top: 10px;">
                        <label>Tipe Karyawan</label>
                        <select name="tipe_karyawan" class="form-control">
                            <option value="internal">Internal</option>
                            <option value="eksternal">Eksternal</option>
                        </select>
                    </div>

                    <div style="margin-top: 10px;">
                        <label>Password</label><br>
                        <input type="password" name="password" class="form-control">
                    </div>

                    <div style="margin-top: 20px;">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Daftarkan
                            Karyawan
                        </button>
                    </div>


                </form>
            </div>
        </div>
    </div>

<?php include("include/footer.php"); ?>
