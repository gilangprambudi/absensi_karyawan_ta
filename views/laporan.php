<?php
session_start();
include("./../scripts/koneksi_db.php");
//get semua daftar karyawan
//cari data admin di database
$stmt = $pdo->query('SELECT * FROM `employee`');

$employees = [];
while ($row = $stmt->fetch()) {
    $employees[] = $row;
}


$months = ["Januari", "Februari", "Maret", "April", "May",
    "Juni", "Juli", "Agustus", "September", "Oktober",
    "November", "Desember"];

$yearStart = 2019;
$yearEnd = 2022;

include "include/header.php";
?>
<div class="content" style="padding: 20px">
    <div class="row">
        <div class="col-md-12">
            <h2><i class="fa fa-print"></i> Cetak Laporan</h2>
            <table class="table" style="margin-top: 20px">
                <tr>
                    <th>
                        No
                    </th>
                    <th>
                        Nama Laporan
                    </th>

                    <th>
                        Nama Karyawan
                    </th>

                    <th>
                        Tanggal Laporan
                    </th>
                    <th>

                    </th>
                </tr>


                <tr>
                    <td>
                        1
                    </td>
                    <td>
                        Laporan Absensi
                    </td>

                    <td>
                        <form action="/views/pdf/kehadiran_karyawan.php" method="get">
                        <select name="karyawan_id">
                            <?php for ($i = 0; $i < sizeof($employees); $i++) { ?>
                                <option value="<?php echo $employees[$i]['id']; ?>">
                                    <?php echo $employees[$i]['name'] . " (" .
                                        $employees[$i]['nip'] . ")"; ?>
                                </option>
                            <?php } ?>
                        </select>
                    </td>

                    <td>
                        <select name="month">
                            <?php

                            for ($i = 1; $i <= 12; $i++) { ?>
                                <option value="<?php echo $i; ?>"><?php echo $months[$i - 1]; ?></option>
                            <?php } ?>
                        </select>

                        <select name="year">
                            <?php
                            for ($j = $yearStart; $j <= $yearEnd; $j++) { ?>
                                <option value="<?php echo $j; ?>"><?php echo $j; ?></option>
                            <?php } ?>
                        </select>


                    </td>

                    <td>
                        <input type="submit" class="btn btn-primary" value="Cetak"/>

                        </form>
                    </td>
                </tr>


                <tr>
                    <form action="/views/pdf/shift_schedule_all.php">
                        <td>
                            2
                        </td>
                        <td>
                            Jadwal Shift
                        </td>

                        <td>
                            All
                        </td>

                        <td>
                            <select name="month">
                                <?php
                                for ($i = 1; $i <= 12; $i++) { ?>
                                    <option value="<?php echo $i; ?>"><?php echo $months[$i - 1]; ?></option>
                                <?php } ?>
                            </select>

                            <select name="year">
                                <?php
                                for ($j = $yearStart; $j <= $yearEnd; $j++) { ?>
                                    <option value="<?php echo $j; ?>"><?php echo $j; ?></option>
                                <?php } ?>
                            </select>

                        </td>

                        <td>
                            <input type="submit" value="Cetak" class="btn btn-primary">
                        </td>
                    </form>
                </tr>
            </table>


        </div>
    </div>

</div>

<script>
    $(document).ready(function () {
        var now = new Date();
        var month = (now.getMonth() + 1);
        var day = now.getDate();
        if (month < 10)
            month = "0" + month;
        if (day < 10)
            day = "0" + day;
        var today = now.getFullYear() + '-' + month + '-' + day;
        $('.datePickerReport').val(today);
    });
</script>
<?php include("include/footer.php"); ?>
