<?php
session_start();
include("./../scripts/koneksi_db.php");


//get data karyawan by id

$karyawanID = $_GET['karyawan_id'];

//get semua daftar karyawan
//cari data admin di database
$stmt = $pdo->query('SELECT * FROM `employee` WHERE id = ' . $karyawanID);

$employee = null;

if ($row = $stmt->fetch())
{
    $employee = $row;
}

include "include/header.php";
?>

<div class="row">
    <div class="col-md-12" style="margin-top: 40px">
        <h3><i class="fa fa-id-card"></i> Informasi Biodata Karyawan</h3>

        <table class="table" style="margin-top: 40px">
            <tr>
               <td>Nama</td><td><b><?php echo $employee['name'];?></b></td>
            </tr>
            <tr>
                <td>NIK</td><td><b><?php echo $employee['nip'];?></b></td>
            </tr>
            <tr>
                <td>Tenant</td><td><b><?php echo $employee['tenant'];?></b></td>
            </tr>
            <tr>
                <td>Tipe Karyawan</td><td><b><?php echo $employee['tipe_karyawan'];?></b></td>
            </tr>
            <tr>
                <td>Foto KTP</td><td><img src="<?php echo $employee['ktp_url'];?>" width="400px"></td>
            </tr>
            <tr>
                <td>Jadwal Shift</td><td><a href="/views/manajemen_shift_karyawan.php?karyawan_id=<?php echo $employee['id'];?>">Click Disini Kelola Shift</a></td>
            </tr>
        </table>
    </div>
</div>

<?php
include "include/footer.php";
?>
