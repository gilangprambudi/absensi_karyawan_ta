<?php
session_start();
include("./../scripts/koneksi_db.php");
include("./../scripts/int_to_hour.php");
//get semua daftar karyawan
//cari data admin di database
$stmt = $pdo->query('SELECT * FROM `employee`');

$employees = [];
while ($row = $stmt->fetch()) {
    $employees[] = $row;
}


//get tipe shift (pagi, malam, etc)
$index = 0;
$stmt = $pdo->query('SELECT * FROM `shift_change_request` WHERE approved=0');
$tukarShiftList = [];
while ($row = $stmt->fetch()) {

    //get requestee current shift schedule
    $requesteeID = $row['employee_req_id'];
    $targetID = $row['employee_target_id'];
    $requestedDate = $row['date'];


    $stmtReq = $pdo->query("SELECT * FROM `employee` WHERE id=$requesteeID");
    if ($rowEmployee = $stmtReq->fetch()){
        $employeeReq = $rowEmployee;
    }

    $stmtReqShiftSched = $pdo->query("SELECT * FROM `employee_shift` 
            INNER JOIN shift_type ON employee_shift.shift_id = shift_type.id 
            WHERE employee_id = $requesteeID 
            AND date = '$requestedDate'");
    if ($rowEmployeeCurrShift = $stmtReqShiftSched->fetch()){
        $employeeReqCurrShift = $rowEmployeeCurrShift;
    }




    $stmtTarget = $pdo->query("SELECT * FROM `employee` WHERE id=$targetID");
    if ($rowEmployee = $stmtTarget->fetch()){
        $employeeTarget = $rowEmployee;
    }

    $stmtTargetShiftSched = $pdo->query("SELECT * FROM `employee_shift` 
            INNER JOIN shift_type ON employee_shift.shift_id = shift_type.id 
            WHERE employee_id = $targetID 
            AND date = '$requestedDate'");
    if ($rowEmployeeCurrShift = $stmtTargetShiftSched->fetch()){
        $employeeTargetCurrShift = $rowEmployeeCurrShift;
    }

    $tukarShiftList[$index] = $row;
    $tukarShiftList[$index]['employee_req'] = $employeeReq;
    $tukarShiftList[$index]['employee_req_curr_shift'] = $employeeReqCurrShift;
    $tukarShiftList[$index]['employee_target_curr_shift'] = $employeeTargetCurrShift;
    $tukarShiftList[$index]['employee_target'] = $employeeTarget;
}

include "include/header.php";
?>

    <style>
        table{
            font-size: 12px;
        }

        .red-label{
            background-color: #fe7171;
        }
    </style>
    <link href='../assets/fullcalendar/lib/main.css' rel='stylesheet'/>
    <script src='../assets/fullcalendar/lib/main.js'></script>
    <script>

        document.addEventListener('DOMContentLoaded', function () {
            var calendarEl = document.getElementById('calendar');
            var calendar = new FullCalendar.Calendar(calendarEl, {
                initialView: 'dayGridMonth'
            });
            calendar.render();

            calendar.on('dateClick', function (info) {
                window.location = "manajemen_shift.php?date=" + info.dateStr;
            });
        });

    </script>
    <div style="padding: 20px">
        <div class="row" style="margin-bottom: 60px">
            <div class="col-md-12" style="margin-bottom: 30px">
                <h3>
                    <i class="fa fa-paper-plane"></i> Permintaan Tukar Shift
                </h3>

            </div>
            <div class="col-md-12">



                <div class="card" style="padding: 10px">
                    <div style="margin-top: 20px">

                        <div>Keterangan : </div>

                        <span style="width: 25px; height: 25px;
                         display: inline-block; background-color: #fe7171;
                         margin: 20px;"></span>
                        <span style="position:absolute; margin-top: 20px; margin-left: -10px;">Belum Di Approve</span>
                    </div>

                    <table class="table table-bordered">
                        <tr>
                            <th>No</th><td>Nama Karyawan</td>
                            <td>Tanggal</td>
                            <td>Shift Awal</td><td>Shift Setelah Ditukar</td>
                            <td>Shift Yang Ditukar</td><td>Tindak Lanjut</td>
                        </tr>

                        <?php
                            for($i = 0; $i < sizeof($tukarShiftList); $i++){
                        ?>
                                <tr class="<?php if(!$tukarShiftList[$i]['approved']){ echo "red-label"; }?>">
                                    <td><?php echo $i + 1;?></td>
                                    <td><?php echo $tukarShiftList[$i]['employee_req']['name']  . " ( " . $tukarShiftList[$i]['employee_req']['nip'] . " )";?></td>
                                    <td><?php echo $tukarShiftList[$i]['date'];?></td>
                                    <td><?php echo $tukarShiftList[$i]['employee_req_curr_shift']['name'];?>
                                        ( <?php echo intToHour($tukarShiftList[$i]['employee_req_curr_shift']['time_start']) . "-" . intToHour($tukarShiftList[$i]['employee_req_curr_shift']['time_end']) ?> )</td>
                                    <td><?php echo $tukarShiftList[$i]['employee_target_curr_shift']['name'];?>
                                        ( <?php echo intToHour($tukarShiftList[$i]['employee_target_curr_shift']['time_start']) . "-" . intToHour($tukarShiftList[$i]['employee_target_curr_shift']['time_end']) ?> )</td>
                                    <td><?php echo $tukarShiftList[$i]['employee_target']['name'] . " ( " . $tukarShiftList[$i]['employee_target']['nip'] . " )";?></td>
                                    <td>
                                        <form method="POST" action="/scripts/approve_tukar_shift.php">
                                            <input type="hidden" name="id" value="<?php echo $tukarShiftList[$i]['id']; ?>">
                                            <input type="submit" class="btn btn-success" value="Terima">
                                        </form>

                                        <form method="POST" action="/scripts/reject_request_tukar_shift.php">
                                            <input type="hidden" name="id" value="<?php echo $tukarShiftList[$i]['id']; ?>">
                                            <input type="submit" class="btn btn-danger" value="Tolak">
                                        </form>
                                    </td>
                                </tr>
                        <?php } ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
<?php include "include/footer.php" ?>
