<?php
session_start();
include("./koneksi_db.php");

//get post data
$shiftDate = $_POST['shiftDate'];
$shiftType = $_POST['shiftType'];
$employeeId = $_POST['employeeId'];

//check if the date is already occupied
$query = 'SELECT id FROM employee_shift WHERE date BETWEEN "'. $shiftDate .'" AND "' .
    $shiftDate . '" AND employee_id=' . $employeeId;
$stmt = $pdo->query($query);
if ($row = $stmt->fetch()){
    $_SESSION['date_occupied'] = true;
    header("location: /views/manajemen_shift_karyawan.php?karyawan_id=".$employeeId);
    return;
}


$query = 'INSERT INTO employee_shift (employee_id, shift_id, date) VALUES(?, ?, ?)';
$stmt = $pdo->prepare($query);
$stmt->execute([$employeeId, $shiftType, $shiftDate]);

return header("location: /views/manajemen_shift_karyawan.php?karyawan_id=".$employeeId);
