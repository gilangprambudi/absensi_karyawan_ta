<?php
include("./koneksi_db.php");

//get post data
$nip = $_POST['nip'];
$name = $_POST['name'];
$tenant = $_POST['tenant'];
$password = $_POST['password'];
$tipeKaryawan = $_POST['tipe_karyawan'];

$target_dir = "../uploads/";
$curDateSec = date("h:i:sa");
$target_file = $target_dir . $curDateSec . basename($_FILES["foto_ktp"]["name"]);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
// Check if image file is a actual image or fake image
$check = getimagesize($_FILES["foto_ktp"]["tmp_name"]);
if($check !== false) {
    echo "File is an image - " . $check["mime"] . ".";
    $uploadOk = 1;
} else {
    echo "File is not an image.";
    $uploadOk = 0;
}



// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["foto_ktp"]["tmp_name"], $target_file)) {
        echo "The file ". basename( $_FILES["foto_ktp"]["name"]). " has been uploaded.";
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}

$uploadedFileName = $target_file;

$passwordHashed = password_hash($password, PASSWORD_DEFAULT);
$query = 'INSERT INTO employee (nip, name, tenant, ktp_url, password, tipe_karyawan) VALUES(?, ?, ?, ?, ?, ?)';
$stmt = $pdo->prepare($query);
$stmt->execute([$nip, $name, $tenant, $target_file, $passwordHashed, $tipeKaryawan]);

return header("location: /views/manajemen_karyawan.php");
