<?php
include("koneksi_db.php");
session_start();

$karyawanRequesteeID = $_SESSION["karyawan"]["id"];
$karyawanTargetID = $_POST["target_id"];
$date = $_POST["req_date"];

$query = 'INSERT INTO shift_change_request (employee_req_id, employee_target_id, 
approved, date) VALUES(?, ?, 0, ?)';
$stmt = $pdo->prepare($query);
$stmt->execute([$karyawanRequesteeID, $karyawanTargetID, $date]);

return header("location: /views/karyawan-request-tukar-shift.php");
