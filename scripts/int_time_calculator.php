<?php

function getMinuteRange($time1, $time2){
    //get hour and minute
    $time1Hour = 0;
    $time1Minute = 0;

    $time2Hour = 0;
    $time2Minute = 0;

    $time1Minute = $time1 % 100;
    $time2Minute = $time2 % 100;

    $time1Hour = floor($time1 / 100);
    $time2Hour = floor($time2 / 100);

    $timeRangeHour = abs($time1Hour - $time2Hour) * 60;
    return abs($time1Minute - $time2Minute) + $timeRangeHour;
}
