<?php
session_start();
include("koneksi_db.php");

//validasi data post
if (!isset($_POST['nip']) ||
    !isset($_POST['password'])){
    $_SESSION['error_login'] = "Tolong masukkan username dan password";
    return header("Location: /views/login_karyawan.php");
}

//data post form
$username = $_POST['nip'];
$password = $_POST['password'];

//cari data admin di database
$stmt = $pdo->query('SELECT * FROM `employee` WHERE nip="'.$username.'"');
if (!($row = $stmt->fetch()))
{
    $_SESSION['error_login'] = "Username atau Password salah";
    return header("Location: /views/login_karyawan.php");
}

//data ditemukan, compare password
$admin = $row;

if(!password_verify($password, $admin['password'])){
    $_SESSION['error_login'] = "Username atau Password salah";
    return header("Location: /views/login_karyawan.php");
}

//password sesuai, alihkan ke halaman dashboard
$_SESSION['karyawan'] = $admin;
return header("Location: /views/dashboard_karyawan.php");