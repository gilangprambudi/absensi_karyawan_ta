<?php

function intToHour($i){
    $hour = floor($i/100);
    $minute = $i % 100;

    if($hour < 10){
        $hour = "0".$hour;
    }

    if($minute < 10){
        $minute = "0".$minute;
    }


    return $hour . ":" . $minute;
}