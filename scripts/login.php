<?php
session_start();
include("koneksi_db.php");

//validasi data post
if (!isset($_POST['username']) || 
!isset($_POST['password'])){
    $_SESSION['error_login'] = "Tolong masukkan username dan password";
    return header("Location: /views/login_admin.php");
}

//data post form
$username = $_POST['username'];
$password = $_POST['password'];

//cari data admin di database
$stmt = $pdo->query('SELECT * FROM `admin` WHERE username="'.$username.'"');
if (!($row = $stmt->fetch()))
{   
    $_SESSION['error_login'] = "Username atau Password salah";
    return header("Location: /views/login_admin.php");
}

//data ditemukan, compare password
$admin = $row;

if(!password_verify($password, $admin['password'])){
    $_SESSION['error_login'] = "Username atau Password salah";
    return header("Location: /login_admin.php");
}

//password sesuai, alihkan ke halaman dashboard
$_SESSION['admin'] = $admin;
return header("Location: /views/dashboard_admin.php");