<?php
session_start();
include("./koneksi_db.php");

//get data from post
$nip_karyawan = $_POST['nip'];


//get data karyawan dari database bedasarkan nip
$stmt = $pdo->query('SELECT * FROM `employee` WHERE nip = "' . $nip_karyawan . '"');
$employee = null;
if ($row = $stmt->fetch())
{
    $employee = $row;
}

//cek apakah karywan ada
if($employee == null){
    $_SESSION['attendance_employee_not_exist'] = true;
    header("Location: /views/form_absensi.php");
    return;
}


//get current time
$date = date("Y-m-d");
$currentTime = date('Hi');

//Cek apakah karyawan hadir didalam waktu shift
$stmt = $pdo->query('SELECT employee_shift.shift_id, employee.id, employee.name, employee.nip 
FROM employee_shift INNER JOIN employee on employee_shift.employee_id = employee.id
WHERE employee.nip = "' . $nip_karyawan .
    '" AND employee_shift.date BETWEEN "'. $date .'" AND "' . $date . '"');
$employeeShiftSchedule = [];

while($row = $stmt->fetch()){
    $employeeShiftSchedule[] = $row;
}

if(sizeof($employeeShiftSchedule) < 1){
    $_SESSION['attendance_employee_has_no_schedule_today'] = true;
    header("Location: /views/form_absensi.php");
    return;
}

//cek apakah karyawan telah melakukan absensi masuk hari ini
$stmt = $pdo->query('SELECT id FROM `employee_attendance` WHERE employee_id = ' . $employee['id'] . ' AND attendance_date BETWEEN "'.
    $date .'" AND "' . $date . '"');
$employeeAttendance = null;
if($row = $stmt->fetch()){
    $employeeAttendance = $row;
}

if($employeeAttendance != null){

    //jika sudah melakukan absensi, maka catat waktu absensi sekarang sebagai waktu keluar
    $employeeId = $employee['id'];
    $stmt = $pdo->prepare("UPDATE `employee_attendance` SET attendance_out = :attendance_out WHERE employee_id = $employeeId 
    AND attendance_date BETWEEN '$date' AND '$date'");
    $stmt->bindParam(":attendance_out", $currentTime);
    $stmt->execute();

    $_SESSION['attendance_error'] = "karyawan " . $employee['nip'] . " telah melakukan absen keluar hari ini";
    header("Location: /views/form_absensi.php");
    return;
}


//Tambah data absensi karyawan
$stmt = $pdo->prepare('INSERT INTO `employee_attendance` (employee_id, 
attendance_date, attendance_in) 
VALUES (:employee_id, :attendance_date, :attendance_in)');
$stmt->bindParam(':employee_id', $employee['id']);
$stmt->bindParam(":attendance_date", $date);
$stmt->bindParam(":attendance_in",$currentTime);
$stmt->execute();


$_SESSION['isAbsen'] = true;
$_SESSION['nik_absen'] = $employee['nip'];

//redirect back to Form Absensi
header("Location: /views/form_absensi.php");
